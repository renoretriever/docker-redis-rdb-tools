FROM alpine:latest

MAINTAINER renoretriever <renoretriever@gmail.com>

RUN set -x && \
    apk update && \
    apk --update add \
        gcc \
        musl-dev \
        python3 \
        python3-dev \
        git && \
    pip3 install \
        rdbtools \
        python-lzf

WORKDIR /root/
